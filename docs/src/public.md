# API Reference

For detailed examples, please refer to section "[Examples](@ref)".

```@docs
energyplot
pressureplot
bulkmodulusplot
equationsplot
```
